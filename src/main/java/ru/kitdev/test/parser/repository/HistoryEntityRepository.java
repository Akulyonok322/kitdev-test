package ru.kitdev.test.parser.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kitdev.test.parser.model.HistoryEntity;

@Repository
public interface HistoryEntityRepository extends JpaRepository<HistoryEntity, Long> {
}
