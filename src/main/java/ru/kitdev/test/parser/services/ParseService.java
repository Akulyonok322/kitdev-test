package ru.kitdev.test.parser.services;


import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.kitdev.test.parser.model.HistoryEntity;
import ru.kitdev.test.parser.services.db.HistoryService;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Log4j2
@Service
@RequiredArgsConstructor
public class ParseService {

    private final HistoryService historyService;

    private static final Pattern urlPattern = Pattern.compile("https?://(www\\.)?[-a-zA-Z0-9@:%._+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_+.~#?&/=]*)");

    /**
     * Циклический запуск раз в 30 сек
     */
    @Scheduled(fixedRateString = "30000")
    private void startParse() {
        //получаем список файлов в папке /fileToParse
        Set<String> fileList = getFilesFromDir("./fileToParse");
        if (fileList.isEmpty()){
            log.debug("В папке ./fileToParse отсутствуют фвйлы");
            return;
        }
        for (String file : fileList) {
            // Получаем URL и список паттернов для него
            Map<URL, List<Pattern>> urlAndPatterns = readFile("./fileToParse/" + file);
            if (!urlAndPatterns.isEmpty()) {
                for (Map.Entry<URL, List<Pattern>> entryUrl : urlAndPatterns.entrySet()) {
                    // Для каждого URL и паттерна для него получаем количество соответствий
                    Map<Pattern, Long> patternMatches = countOfMatchers(entryUrl.getKey(), entryUrl.getValue());
                    // перебираем список паттернов и количества соответствий для сохранения в БД
                    for (Map.Entry<Pattern, Long> entryPattern : patternMatches.entrySet()) {
                        HistoryEntity history = HistoryEntity.builder()
                                .pattern(entryPattern.getKey().toString())
                                .countOfMatching(entryPattern.getValue())
                                .url(entryUrl.getKey().toString())
                                .fileName(file)
                                .timestamp(LocalDateTime.now())
                                .build();
                        historyService.save(history);
                    }
                }
            }
        }
    }

    public Set<String> getFilesFromDir(String dir) {
        try (Stream<Path> stream = Files.list(Paths.get(dir))) {
            return stream
                    .filter(file -> !Files.isDirectory(file))
                    .map(Path::getFileName)
                    .map(Path::toString)
                    .collect(Collectors.toSet());
        } catch (IOException e) {
            log.error("Не удалось получить список файлов в директории: " + dir);
            return new HashSet<>();
        }
    }


    public Map<URL, List<Pattern>> readFile(String uri) {

        Map<URL, List<Pattern>> urlAndPatterns = new HashMap<>();

        URL lastReadURL = null;
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(uri));
        } catch (FileNotFoundException e) {
            log.error("Не удалось прочитать файл - " + uri + "\n" + e);
        }
        String line;
        try {
            while ((line = br.readLine()) != null) {

                // если строка это URL то записываем в key urlAndPatterns
                if (urlPattern.matcher(line).matches()) {
                    lastReadURL = new URL(line);
                    urlAndPatterns.put(lastReadURL, new ArrayList<>());
                    log.trace("New URL found: " + lastReadURL);
                }
                // все строки, после URL, не являющиеся URL считаю паттернами для данного URL
                // добавляю паттерн в список к последнему прочитанному из файла URL
                else {
                    if (lastReadURL != null) {
                        urlAndPatterns.get(lastReadURL).add(Pattern.compile(line));
                        log.trace("Pattern: " + line + "\nadd to URL: " + lastReadURL);
                    }
                    // если паттерн идёт до URL
                    else {
                        log.warn("Pattern: " + line + "was skip. URL before pattern required");
                    }
                }
            }
        } catch (MalformedURLException e) {
            log.error("Не получилось распознать URL " + e);
        } catch (IOException e) {
            log.error(e.toString());
        }
        try {
            br.close();
        } catch (IOException e) {
            log.error("Не получилось закрыть буферРидер " + e);
        }
        return urlAndPatterns;
    }

    private Map<Pattern, Long> countOfMatchers(URL url, List<Pattern> patterns) {
        Map<Pattern, Long> result = new HashMap<>();
        Document doc;
        try {
            doc = Jsoup.connect(url.toString()).get();
        } catch (IOException e) {
            log.error("Не получилось подключиться к URL - " + url + "\n" + e);
            return result;
        }
        for (Pattern pattern : patterns) {
            long i = pattern.matcher(doc.data()).results().count();
            result.put(pattern, i);
            log.trace("По адресу: " + url + "\nНайдено " + i + " совпадений по паттерну: " + pattern);
        }
        return result;
    }


}
