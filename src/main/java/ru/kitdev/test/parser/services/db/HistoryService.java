package ru.kitdev.test.parser.services.db;

import org.springframework.stereotype.Service;
import ru.kitdev.test.parser.model.HistoryEntity;
import ru.kitdev.test.parser.repository.HistoryEntityRepository;

@Service
public class HistoryService {

    private final HistoryEntityRepository historyRepo;

    public HistoryService(HistoryEntityRepository historyRepo) {
        this.historyRepo = historyRepo;
    }

    public boolean save(HistoryEntity history) {
        historyRepo.save(history);
        return true;
    }
}
