package ru.kitdev.test.parser.model;

import javax.persistence.*;

import lombok.*;

import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "History")
public class HistoryEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    private String fileName;
    @Column(columnDefinition = "TEXT")
    private String url;
    private String pattern;
    private Long countOfMatching;
    @Column(columnDefinition = "datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP()")
    private LocalDateTime timestamp;




}
