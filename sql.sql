CREATE TABLE Products (
	Id bigint PRIMARY KEY,
	Name TEXT
);

INSERT INTO Products
VALUES
	(1, 'Vinil oboi'),
	(2, 'Plitka'),
	(3, 'Laminat'),
	(4, 'Perforator'),
	(5, 'Kley dlya oboev'),
	(6, 'Nechto');

CREATE TABLE Categories (
	Id bigint PRIMARY KEY,
	Name TEXT
);

INSERT INTO Categories
VALUES
	(1, 'Oboi'),
	(2, 'Napolnye pokrytia'),
	(3, 'Tools'),
	(4, 'Rashodniki');


CREATE TABLE ProductCategories (
	ProductId bigint FOREIGN KEY REFERENCES Products(Id),
	CategoryId bigint FOREIGN KEY REFERENCES Categories(Id),
	PRIMARY KEY (ProductId, CategoryId)
);

INSERT INTO ProductCategories
VALUES
	(1, 1),
	(2, 2),
	(3, 2),
	(4, 4),
	(5, 1),
	(5, 4);

SELECT P."Name", C."Name"
FROM Products P
LEFT JOIN ProductCategories PC
	ON P.Id = PC.ProductId
LEFT JOIN Categories C
	ON PC.CategoryId = C.Id;