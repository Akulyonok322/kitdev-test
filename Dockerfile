FROM openjdk:17-alpine
COPY ./target/parser-0.0.1-SNAPSHOT.jar /app/parser-app.jar
COPY ./fileToParse/ /app/fileToParse
WORKDIR /app/
ENTRYPOINT ["java","-jar","parser-app.jar"]