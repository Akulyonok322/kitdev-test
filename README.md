Для запуска приложения достаточно только файла docker-compose.yaml
В папке где запущен `docker compose up` появится папка с наименованием fileToParse
В эту папку нужно закинуть файлы, с определённым содержимым:
 - строка содержащая URL
 - после URL строки с паттернами, для данного URL
   Например:
```
https://www.detmir.ru/catalog/index/name/sortforsale/is_sale/1/
Подгузники
\d+\s
\w+\s
https://www.youtube.com/
просмотров
https://yandex.ru/jobs/vacancies?professions=backend-developer&professions=database-developer&professions=desktop-developer&professions=frontend-developer&professions=full-stack-developer&professions=mob-app-developer&professions=mob-app-developer-android&professions=mob-app-developer-ios&professions=noc-developer&professions=system-developer
java
```

Приложение раз в 30 сек вычитывает все файлы из этой папки и парсит указанные сайты.

Приложение использует БД MariaDB. 

Для просмотра БД добавлен контейнер Adminer.  
Подключение через браузер по адресу http://localhost:8080/
- сервер: db
- user: root
- pass: root
- db: parser

Выбрать таблицу history  

|  id | count_of_matching | file_name |   pattern  |      timestamp      |                                                  url                                                  |
|:---:|:-----------------:|:---------:|:----------:|:-------------------:|:-----------------------------------------------------------------------------------------------------:|
| 181 | 61                | testFile1 | \d+\s      | 2022-09-30 18:28:57 | https://www.detmir.ru/catalog/index/name/sortforsale/is_sale/1/                                       |
| 182 | 1                 | testFile  | Сырники    | 2022-09-30 18:28:57 | https://eda.ru/recepty                                                                                |
| 183 | 125               | testFile1 | просмотров | 2022-09-30 18:29:26 | https://www.youtube.com/                                                                              |
| 184 | 5                 | testFile1 | java       | 2022-09-30 18:29:27 | https://yandex.ru/jobs/vacancies?professions=backend-developer&professions=database-developer&profes… |
| 185 | 61                | testFile1 | \d+\s      | 2022-09-30 18:29:28 | https://www.detmir.ru/catalog/index/name/sortforsale/is_sale/1/                                       |
| 186 | 5                 | testFile1 | Подгузники | 2022-09-30 18:29:28 | https://www.detmir.ru/catalog/index/name/sortforsale/is_sale/1/                                       |
| 187 | 949               | testFile1 | \w+\s      | 2022-09-30 18:29:28 | https://www.detmir.ru/catalog/index/name/sortforsale/is_sale/1/                                       |
| 188 | 1                 | testFile  | Сырники    | 2022-09-30 18:29:28 | https://eda.ru/recepty                                                                                |
| 189 | 71                | testFile1 | просмотров | 2022-09-30 18:29:55 | https://www.youtube.com/                                                                              |

![img.png](img.png)

БД при перезапуске контейнера сохраняется. 

Логи в папке /logs там где был запущен `docker compose up`

Недостатки:
- Со структурой БД заморачиваться не стал. Как логгер использую. Всё в одной таблице.
- Выбрал html парсер сайтов, который не читает половину c JS сайтов. По хорошему нужна библиотека селениума с вебдрайвером ...


SQL запрос в файле sql.sql в корне проекта


# ТЗ

## Java Task

Задача сервис — подсчёт  количества определенных  паттернов (слов) на целевых ресурсах (источники входных параметров: файлы в определенной папке с построчным списком URL и паттернов поиска )  с заданной периодичностью.

Результат записывается в базу данных.

Реализация на net.core  в виде отдельного сервиса работающего в фоновом режиме.
Простой вариант - парсер по запуску с таймером (GUI не важен).


## SQL Task

В базе данных MS SQL Server есть продукты и категории. Одному продукту может соответствовать много категорий, в одной категории может быть много продуктов. Напишите SQL запрос для выбора всех пар «Имя продукта – Имя категории». Если у продукта нет категорий, то его имя все равно должно выводиться.